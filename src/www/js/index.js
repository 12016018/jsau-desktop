'use strict'

const my_code = require('../js/mycode')

document.getElementById('myButton').addEventListener('click', function() {
    const message_pane = document.getElementById('message-pane')
    const chemin = document.getElementById('chemin').value // le chemin de notre rep choisi
    my_shared_code.parcourir(chemin, function(err, results) {
          if (err) throw err
            my_code.envoyer(chemin, results, message_pane)
    })
})
