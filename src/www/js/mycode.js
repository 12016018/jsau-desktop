'use strict'

var  fetch = require('node-fecth')
var  fs = require('fs')

/**
* Affiche les noms des fichiers au sein du répertoire dir de façon récursive
* @param dir le repertoire choisi
* @param done callback
*/
function parcourir(dir, done) {
  var liste = {}
  liste[dir] = []

  fs.readdir(dir, function(err, list) {
    if (err) {
      return done(err)
    }


    var i = 0
    var next = function () {
      var file = list[i++]
      if (!file) {
        return done(null, liste) //
      }
      file = dir + '/' + file
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) { //un nouveau parcour si un dossier
          liste[dir].push(file.substr(dir.length+1, file.length) + '/') //:Ajout du dossier dans la liste
          parcourir(file, function(err, res) {

            for (var indice in res) {
              liste[indice] = res[indice];
            }
            next()
          })
        }
        else { //On ajoute le fichier au résultat
          liste[dir].push(file.substr(dir.length+1, file.length))
          next()
        }
      })
    }
    next()
  })
}

function envoyer(chemin, arbre, elem) {
    //Envoie à l'apiserver
    fetch('https://web-2.local.test/api', {
        p1: chemin,
        p2: arbre
    })
      .then(function(response) {
        var  str = JSON.stringify(response.data, null, '\t')
        elem.innerHTML = '<pre>' + str + '</pre>'
      })
      .catch(function(error) {
        
      })
}

module.exports = {
    parcourir:parcourir,
    envoyer:envoyer
}
